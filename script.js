const table = document.createElement('table');
for (let i = 0; i < 30; i++) {
	const row = document.createElement('tr');
	for (let j = 0; j < 30; j++) {
		const cell = document.createElement('td');
		row.appendChild(cell);
	}
	table.appendChild(row);
}

document.body.appendChild(table);

function handleCellClick(event) {
	const target = event.target;
	if (target.tagName === 'TD') {
		if (target.style.backgroundColor === 'black') {
			target.style.backgroundColor = 'white';
		} else {
			target.style.backgroundColor = 'black';
		}
	}
}

table.addEventListener('click', handleCellClick);

function handleBodyClick(event) {
	if (!table.contains(event.target)) {
		const cells = document.getElementsByTagName('td');
		for (let i = 0; i < cells.length; i++) {
			const cell = cells[i];
			if (cell.style.backgroundColor === 'black') {
				cell.style.backgroundColor = 'white';
			} else {
				cell.style.backgroundColor = 'black';
			}
		}
	}
}


document.body.addEventListener('click', handleBodyClick);